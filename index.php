<?php
require_once('class/autoload.php');

/*
 * Callback: 
 array
 response: string,
 error_msg: string,
*/

// RECAPTCHA START
// ======================================
$is_recaptcha_ok = 0;
$site_key_recaptcha = '6Lee7WweAAAAACgctSMqT9Cd6jMipg6kG_iFSP3Y';
$secret_key_recaptcha = '6Lee7WweAAAAAP93f02R2dGdGwQeCCmFuHcZ4TlX';

$post = NULL;
if ($_POST) 
{
	$post = $_POST;
	
	$is_recaptcha_ok = -1;
	$captcha;
	
	$response = $responseKeys = NULL;
	if (isset($_POST['g-recaptcha-response'])){
		$captcha = $_POST['g-recaptcha-response'];

		$ip = $_SERVER['REMOTE_ADDR'];
		// post request to server
		$url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key_recaptcha) .  '&response=' . urlencode($captcha);
		$response = file_get_contents($url);
		$responseKeys = json_decode($response,true);
		
		// should return JSON with success as true
		if (isset($responseKeys["success"]) && $responseKeys["success"] === TRUE && !isset($responseKeys['error-codes'])) {	
			$is_recaptcha_ok = 1;
		}
		
		// Save to log 
		// ----------------------------------------
		// $log = $tmp = NULL;
		// $tmp['raw'] = $url;
		// $tmp['result'] = $responseKeys;
		// $log['source'] = 'contact';
		// $log['subject'] = 'RECAPTCHA';
		// $log['content'] = json_encode($tmp,1);
		// $log['notes'] = '';
		// $this->log_model->save($log);
		
		// debug($responseKeys);
		// debug($is_recaptcha_ok);
		// die;
	
	}

}
// RECAPTCHA END
// ======================================
$gInfo = NULL;

// DEFAULT RESPONSE
if ($is_recaptcha_ok < 0) $gInfo = "Ooops, your captcha input was wrong. Please try again";
// debug($is_recaptcha_ok);
// die;
if ($is_recaptcha_ok >= 1)
{
	$err_message = NULL;
	if (! isset($post['name'])) {
		$err_message[] = 'Subject harus diisi';
	}
	
	if (! isset($post['email'])) {
		$err_message[] = 'Email harus diisi';
	}
	
	if (! isset($post['captcha'])) {
		$err_message[] = 'Captcha harus diisi';
	}

	$to = $name = $message = NULL;
	$subject = "Webform Metropack";
	$to = $post['tName'];
	$email = $post['tEmail'];
	$message = $post['tMessage'];
	// ------------------------------
	
	if(!isset($to)){
		$gInfo .= "Name must be filled.";
	} elseif (!isset($email)){
		$gInfo .= "Email must be filled.";
	} 
	elseif (!isset($message)){
		$gInfo .= "Message must be filled.";
	} else {
		
		// echo "oke berhasil bro";die;
		// $save = array(
			// 'fullname' => $name,
			// 'email' => $email,
			// 'subject' => $subject,
			// 'message' => $message,
		// );
		// $save = $this->inbox_model->save($save);
		
		$content = '
		<html>
		<body>
		<table style="" cellpadding="15px" cellspacing="0" border="1">
		<tr>
			<td width="80px">Name</td>
			<td>'.$to.'</td>
		</tr>
		<tr>
			<td width="80px">Email</td>
			<td>'.$email.'</td>
		</tr>
		<tr>
			<td width="80px">Message</td>
			<td>'.$message.'</td>
		</tr>
		</table>
		</body>
		</html>
		';
		// debug($content);
		// die;
		
		$config_email = NULL;
			
		/*
		USE CURL
		*/		
		
		// content-type: application/json
		// accept: application/json
		// 
		// $apismtp = "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";
		// $header['api-key'] = $apismtp;
		// $header['smtp_url'] = "https://api.sendinblue.com/v3/smtp/email";
		
		// $url = 'http://localhost/greviacom/api/membernew/get_list';
		// $curl = $CI->curl_sendblue($url,'get','','');
		$is_sent = 0;
		
		// Change accordingly admin
		// $to = 'rusdi.karsandi@gmail.com';
		// $to_alias = 'Rusdi';
		
		$to = 'metropackindonesia@gmail.com';
		$to_alias = 'Admin Metropack Indonesia';
		
		$from = 'info@metropackindonesia.com';
		$from_alias = 'Metropack Web Form';
		
		// ----------------------------------------------
		// SENDING EMAIL START
		$paramcurl = NULL;
		$paramcurl['sender'] = array(
			'name' => $from_alias,
			'email' => $from
			);
		$paramcurl['to'][] = array(
			'name' => $to_alias,
			'email' => $to,
		);
		$paramcurl['subject'] = $subject;
		$paramcurl['htmlContent'] = $content;
		
		$curl = curl_sendblue('post',$paramcurl);
		// SENDING EMAIL END
		// ----------------------------------------------
				
		// Check callback exist or not
		
		if (isset($curl['response']['messageId'])) $is_sent = 1;
		
		// Save to database
		$param = NULL;
		$param['domain'] = 'mpi_local';
		$param['from_alias'] = $from;
		$param['subject'] = $subject;
		$param['content'] = $content;
		$param['to_list'] = $to;
		// $param['cc_list'] = ;
		// $param['bcc_list'] = ;
		// $param['is_html'] = ;
		$param['is_sent'] = $is_sent;
		$param['sent_count'] = 1;
		$param['remarks'] = json_encode($paramcurl);
		if (isset($curl)) $param['log'] = json_encode($curl);
		$param['creator_id'] = 999;
		$param['creator_date'] = get_datetime();
		// $param['debug'] = 1;
		$save = curl_api_grevia('post','emailblast','save',$param);
		
		// $debug = json_decode($save['debug'],1);
		// debug($save);
		// debug($debug);
		// die;
		
		if ($is_sent) 
			$gInfo = "Thank you for your email, we will respond in 2 x 24 hour.";
		else
			$gInfo = "Sorry, error connection. Please try again.";
	}
}

$jsv = '20220213';
$lang = $getlang = NULL;
$getlang = 'en';
if (isset($_GET['lang'])) $getlang = $_GET['lang'];
$lang = getlang($getlang);
// debug('dame',1);
// debug(HR,1);

// Check email post

?>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136661022-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-136661022-1');
</script>


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Metropack Indonesia - One stop packaging System and Solutions</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="img/favicon.ico?v=<?php echo $jsv?>" type="image/x-icon">
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="css/bootstrap.css?v=<?php echo $jsv?>">
<link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css?v=<?php echo $jsv?>">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css" href="css/style.css?v=<?php echo $jsv?>">
<link rel="stylesheet" type="text/css" href="css/nivo-lightbox/nivo-lightbox.css?<?php echo $jsv?>">
<link rel="stylesheet" type="text/css" href="css/nivo-lightbox/default.css?<?php echo $jsv?>">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="css/style_grevia.min.css?v=<?php echo $jsv?>">

<script src='https://www.google.com/recaptcha/api.js?v=<?php echo $jsv?>' async defer></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<script>
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
  

function initMap() {
 
   var contentString = '<div style="color:black"><b>Metropack Indonesia HO</b><br/><br/>Jl Citarik Raya no. 105 A, Jatibaru<br/>Cikarang Timur - Bekasi 17530.</div>';

  var myLatLng = {lat: -6.2732637, lng: 107.2014387};
  
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!'
  });
  
  //-------------------------------------------
  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  // var marker = new google.maps.Marker({
    // position: uluru,
    // map: map,
    // title: 'Uluru (Ayers Rock)'
  // });
  // marker.addListener('click', function() {
    infowindow.open(map, marker);
  // });
}

</script>
<style>
 #map {
	height: 500px;
	width : 100%;
	padding: 15px ;
 }

</style>
	

</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand page-scroll" href="#page-top"><img src="img/logo-36x36.png" /></a> </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <!--<li><a href="#features" class="page-scroll">Features</a></li>-->
        <li><a href="#about" class="page-scroll">About</a></li>
        <!--
		<li><a href="#services" class="page-scroll">Services</a></li>
		-->
        <li><a href="#portfolio" class="page-scroll">Products</a></li>
        <!--
		<li><a href="#testimonials" class="page-scroll">Testimonials</a></li> 
		<li><a href="#team" class="page-scroll">Team</a></li>-->
        <li><a href="#contact" class="page-scroll">Contact</a></li>
        
		<li>
			<a href="?lang=en" class="page-scroll">EN</a>
		</li>
		<li>
			<a href="?lang=id" class="page-scroll">ID</a>
		</li>
		<!--
		<li>
			<div class="page-scroll">
			  <a class="btn btn-primary dropdown-toggle page-scroll" type="button" href="javascript:void(0)" data-toggle="dropdown">Lang
			  <span class="caret"></span></a>
			  <ul class="dropdown-menu">
				<li><a href="?lang=en" class="page-scroll">English</a></li>
				<li><a href="?lang=id" class="page-scroll">Indonesia</a></li>
			  </ul>
			</div>
		</li>
		-->

      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
</nav>
<!-- Header -->
<header id="header">
  <div class="intro">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 intro-text">
            <?php 
			if (isset($gInfo)) echo print_message($gInfo);
			?>
			<h2>Metropack Indonesia</h2>
            <p style="color:#000"><b>One-stop Packaging System & Solutions</b></p>
            <a href="#about" class="btn btn-custom btn-lg page-scroll">Detail</a> </div>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- Features Section -->
<!--
<div id="features" class="text-center">
	<div class="container">
		<div class="col-md-10 col-md-offset-1 section-title">
			<h2>Features</h2>
		</div>
		<div class="row">
			<div class="col-xs-6 col-md-3"> <i class="fa fa-comments-o"></i>
				<h3>Lorem ipsum</h3>
				<p>Lorem ipsum dolor sit amet placerat facilisis felis mi in tempus eleifend pellentesque natoque etiam.</p>
			</div>
			<div class="col-xs-6 col-md-3"> <i class="fa fa-bullhorn"></i>
				<h3>Dolor sit amet</h3>
				<p>Lorem ipsum dolor sit amet placerat facilisis felis mi in tempus eleifend pellentesque.</p>
				</div>
			<div class="col-xs-6 col-md-3"> <i class="fa fa-group"></i>
				<h3>Tempus eleifend</h3>
				<p>Lorem ipsum dolor sit amet placerat facilisis felis mi in tempus eleifend pellentesque natoque etiam.</p>
			</div>
			<div class="col-xs-6 col-md-3"> <i class="fa fa-magic"></i>
				<h3>Pellentesque</h3>
				<p>Lorem ipsum dolor sit amet placerat facilisis felis mi in tempus eleifend pellentesque natoque.</p>
			</div>
		</div>
	</div>
</div>
-->

<!-- About Section -->
<div id="about">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="about-text">
          <h2>About Us</h2>
          <?php echo $lang['p1']; ?>
          <!-- <p>Metropack INDONESIA merupakan salah satu perusahaan yang bergerak dalam bidang industri kemasan yang didirikan untuk berkontribusi dalam memenuhi kebutuhan pasar yang sangat besar. Dengan berbekal ilmu dan pengalaman serta passion, kami selalu siap dalam memberikan pelayanan terhadap kebutuhan Customer dengan Profesional dan Komitmen tinggi.</p> -->

		  <p><?php echo $lang['p2']; ?></p>

		  <ul>
			<li>Head Office / Packaging Division - 
			Jl Citarik Raya no. 105 A, Jatibaru, Cikarang TImur - Bekasi 17530
			</li>
			<li>Offset Printing Division - 
			Jl. Kebun Kosong 6 no 91, Kemayoran - Jakarta Pusat 10630
			</li>
			<li>Advertising Division - 
			Jl. Arya Putra No.8, Serua Indah - Tangerang Selatan
			</li>
		  </ul>

		  <p><?php echo $lang['p3'] ?></p>
		  </div>
		<h3><?php echo $lang['visih1'] ?></h3>
		<p><?php echo $lang['visi1'] ?></p>

		<h3><?php echo $lang['misih1'] ?></h3>
		<p><?php echo $lang['misi1'] ?></p>

          <!--
		  <h3>Why Choose Us?</h3>
          <div class="list-style">
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <ul>
                <li>Lorem ipsum dolor</li>
                <li>Tempor incididunt</li>
                <li>Lorem ipsum dolor</li>
                <li>Incididunt ut labore</li>
              </ul>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <ul>
                <li>Aliquip ex ea commodo</li>
                <li>Lorem ipsum dolor</li>
                <li>Exercitation ullamco</li>
                <li>Lorem ipsum dolor</li>
              </ul>
            </div>
          </div>
		  -->
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Services Section -->
<!--
<div id="services" class="text-center">
  <div class="container">
    <div class="section-title">
      <h2>Our Services</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit duis sed dapibus leonec.</p>
    </div>
    <div class="row">
      <div class="col-md-4"> <i class="fa fa-wordpress"></i>
        <div class="service-desc">
          <h3>Lorem ipsum dolor</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.</p>
        </div>
      </div>
      <div class="col-md-4"> <i class="fa fa-cart-arrow-down"></i>
        <div class="service-desc">
          <h3>Consectetur adipiscing</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
      </div>
      <div class="col-md-4"> <i class="fa fa-cloud-download"></i>
        <div class="service-desc">
          <h3>Lorem ipsum dolor</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"> <i class="fa fa-language"></i>
        <div class="service-desc">
          <h3>Consectetur adipiscing</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.</p>
        </div>
      </div>
      <div class="col-md-4"> <i class="fa fa-plane"></i>
        <div class="service-desc">
          <h3>Lorem ipsum dolor</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.</p>
        </div>
      </div>
      <div class="col-md-4"> <i class="fa fa-pie-chart"></i>
        <div class="service-desc">
          <h3>Consectetur adipiscing</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.</p>
        </div>
      </div>
    </div>
  </div>
</div>
-->

<!-- Gallery Section -->
<div id="portfolio" class="text-center">
  <div class="container">
    <div class="section-title">
      <h2>Our Products</h2>
      <p>Berikut adalah produk-produk kami.</p>
	  <img src="img/products-min.jpg" class="img img-responsive"/>
	  
    </div>
	<!--
    <div class="row">
      <div class="portfolio-items">
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/01-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Lorem Ipsum</h4>
              </div>
              <img src="img/portfolio/01-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/02-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Adipiscing Elit</h4>
              </div>
              <img src="img/portfolio/02-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/03-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Lorem Ipsum</h4>
              </div>
              <img src="img/portfolio/03-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/04-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Lorem Ipsum</h4>
              </div>
              <img src="img/portfolio/04-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/05-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Adipiscing Elit</h4>
              </div>
              <img src="img/portfolio/05-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/06-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Dolor Sit</h4>
              </div>
              <img src="img/portfolio/06-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/07-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Dolor Sit</h4>
              </div>
              <img src="img/portfolio/07-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/08-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Lorem Ipsum</h4>
              </div>
              <img src="img/portfolio/08-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/09-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Adipiscing Elit</h4>
              </div>
              <img src="img/portfolio/09-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
      </div>
    </div>
	-->
  </div>
</div>
<!-- Testimonials Section -->
<!--
<div id="testimonials">
  <div class="container">
    <div class="section-title text-center">
      <h2>What our clients say</h2>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="testimonial">
          <div class="testimonial-image"> <img src="img/testimonials/01.jpg" alt=""> </div>
          <div class="testimonial-content">
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."</p>
            <div class="testimonial-meta"> - John Doe </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="testimonial">
          <div class="testimonial-image"> <img src="img/testimonials/02.jpg" alt=""> </div>
          <div class="testimonial-content">
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis."</p>
            <div class="testimonial-meta"> - Johnathan Doe </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="testimonial">
          <div class="testimonial-image"> <img src="img/testimonials/03.jpg" alt=""> </div>
          <div class="testimonial-content">
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."</p>
            <div class="testimonial-meta"> - John Doe </div>
          </div>
        </div>
      </div>
      <div class="row"> </div>
      <div class="col-md-4">
        <div class="testimonial">
          <div class="testimonial-image"> <img src="img/testimonials/04.jpg" alt=""> </div>
          <div class="testimonial-content">
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."</p>
            <div class="testimonial-meta"> - Johnathan Doe </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="testimonial">
          <div class="testimonial-image"> <img src="img/testimonials/05.jpg" alt=""> </div>
          <div class="testimonial-content">
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."</p>
            <div class="testimonial-meta"> - John Doe </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="testimonial">
          <div class="testimonial-image"> <img src="img/testimonials/06.jpg" alt=""> </div>
          <div class="testimonial-content">
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis."</p>
            <div class="testimonial-meta"> - Johnathan Doe </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
-->

<!-- Team Section -->
<!--
<div id="team" class="text-center">
  <div class="container">
    <div class="col-md-8 col-md-offset-2 section-title">
      <h2>Meet the Team</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit duis sed dapibus leonec.</p>
    </div>
    <div id="row">
      <div class="col-md-3 col-sm-6 team">
        <div class="thumbnail"> <img src="img/team/01.jpg" alt="..." class="team-img">
          <div class="caption">
            <h4>John Doe</h4>
            <p>Director</p>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 team">
        <div class="thumbnail"> <img src="img/team/02.jpg" alt="..." class="team-img">
          <div class="caption">
            <h4>Mike Doe</h4>
            <p>Senior Designer</p>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 team">
        <div class="thumbnail"> <img src="img/team/03.jpg" alt="..." class="team-img">
          <div class="caption">
            <h4>Jane Doe</h4>
            <p>Senior Designer</p>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 team">
        <div class="thumbnail"> <img src="img/team/04.jpg" alt="..." class="team-img">
          <div class="caption">
            <h4>Karen Doe</h4>
            <p>Project Manager</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
-->


<!-- Contact Section -->
<div id="contact">
  <div class="container">
    <div class="col-md-8">
      <div class="row">
        <div class="section-title">
          <h2>Get In Touch</h2>
          <p>Please fill out the form below to send us an email and we will get back to you as soon as possible.</p>
        </div>
		<?php
		if (isset($gInfo)) echo print_message($gInfo);
		?>
        <form name="sentMessage" id="contactForm" method="post" action="index?#contact">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" id="name" name="tName" class="form-control" placeholder="Name" required="required">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input type="email" id="email" name="tEmail" class="form-control" placeholder="Email" required="required">
                <p class="help-block text-danger"></p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <textarea name="tMessage" id="message" class="form-control" rows="4" placeholder="Message" required></textarea>
            <p class="help-block text-danger"></p>
          </div>
          <div id="success"></div>
		  
		  <div class="g-recaptcha" data-sitekey="<?php echo $site_key_recaptcha; ?>"></div>
		  
          <button type="submit" class="btn btn-custom btn-lg">Send Message</button>
        </form>
      </div>
    </div>
    <div class="col-md-3 col-md-offset-1 contact-info">
      <div class="contact-item">
        <h3>Contact Info</h3>
		<p>
		<span><i class="fa fa-user"></i> Business Navigation</span>
		David Zhang<br/>
		</p>
		
		<p>
		<span><i class="fa fa-phone"></i> Phone</span> 
		+622128511706<br/>
		081388375331</p>
		
        <p><span><i class="fa fa-envelope-o"></i> Email</span> metropackindonesia@gmail.com</p>
		
        <p><span><i class="fa fa-map-marker"></i> Address</span>
		  Jl Citarik Raya no. 105 A, Jatibaru, <br>
          Cikarang TImur - Bekasi 17530</p>
      </div>
    </div>
    
	<div class="col-md-12">
		<div id="map"></div>
    </div>
	
    <div class="col-md-12">
      <div class="row">
        <div class="social">
          <ul>
            <li><a href="https://facebook.com/metropackindonesia" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i>@metropackindonesia</a></li>
            <li><a href="https://www.instagram.com/metropack_indonesia" target="_blank" rel="nofollow"><i class="fa fa-instagram"></i> @metropack_indonesia</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Footer Section -->
<div id="footer">
  <div class="container text-center">
    <p>&copy; 2019 Powered by <a href="http://www.grevia.com?ref=metropackindonesia" rel="nofollow">Grevia Labs</a></p>
  </div>
</div>
<script type="text/javascript" src="js/jquery.1.11.1.js?<?php echo $jsv?>"></script> 
<script type="text/javascript" src="js/bootstrap.js?<?php echo $jsv?>"></script> 
<script type="text/javascript" src="js/SmoothScroll.js?<?php echo $jsv?>"></script> 
<script type="text/javascript" src="js/nivo-lightbox.js?<?php echo $jsv?>"></script> 
<script type="text/javascript" src="js/jqBootstrapValidation.js?<?php echo $jsv?>"></script> 
<script type="text/javascript" src="js/main.js?<?php echo $jsv?>"></script>

<!--<script type="text/javascript" src="js/contact_me.js?<?php echo $jsv?>"></script>--> 

<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNBifdMEWimVBn9YmqoPzluMrccOuWLQA&signed_in=true&libraries=places&callback=initMap">
</script>

<script>
$(document).ready(function(){
	$('img').addClass('img-responsive');
})
</script>

<!-- RECAPTCHA START -->
<script type="text/javascript">
  var onloadCallback = function() {
    console.log("grecaptcha is ready!");
  };
</script>

<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer>
</script>
<!-- RECAPTCHA END -->

</body>
</html>