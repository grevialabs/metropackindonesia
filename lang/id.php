<?php 
return array(
    'p1' => 'Metropack INDONESIA merupakan salah satu perusahaan yang bergerak dalam bidang industri kemasan yang didirikan untuk berkontribusi dalam memenuhi kebutuhan pasar yang sangat besar. Dengan berbekal ilmu dan pengalaman serta passion, kami selalu siap dalam memberikan pelayanan terhadap kebutuhan Customer dengan Profesional dan Komitmen tinggi.',
    'p2' => 'Produk - produk yang kami hasilkan adalah Carton Box, Impraboard, Paper Pallet dan Offset printing beserta komponen pendukung dalam aktivitas pengemasan unttuk Lokal ataupun Exlsport diantaranya Heavy Duty Paper Board, Edge Protector, Corner Protector, Round Protector, Strapping Band, Wrapping, Sticker, Label, dan produk cetakan lainnya serta alat-alat Promosi / Brand Development / Advertising , yang dimana terbagi menjadi 3 divisi yaitu :',
    'p3' => 'Sebagai perusahaan yang menyediakan Onestop Packaging Services, kami bekerjasama dengan <strong>Storopack</strong> yang bergerak dibidang Protection & Cushioning untuk melengkapi performance kami dalam melayani kebutuhan Customer dan Calon Customer.',

    'visih1' => 'VISI',
    'visi1' => 'Menjadi salah satu perusahaan yang selalu exist dan berkembang bahkan yang terbaik dalam Industri Kemasan skala retail maupun partai, untuk terus membantu dan meningkatkan efektivitas dan efisiensi aktivitas pengemasan di Customer secara konsisten dan komitmen tinggi.',

    'misih1' => 'MISI',
    'misi1' => 'Mengembangkan karyawan yang berkompeten dalam menciptakan suasana kerja yang baik dan produktivitas yang optimal. Terus menghadirkan inspirasi untuk menciptakan inovasi-inovasi baru dengan ide-ide kreatif untuk menghasilkan produk berkualitas sesuai kebutuhan pasar. Menghadirkan solusi sebagai salah satu bentuk atau wujud kontribusi/pelayanan yang kami persembahkan kepada customer. Memperluas jaringan pemasaran baik tingkat nasional maupun internasional. Menjalin hubungan kerjasama dan komunikasi yang baik dengan Supplier dan Customer demi terwujudnya kerjasama jangka panjang.',
)
?>