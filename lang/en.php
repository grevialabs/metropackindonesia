<?php 
return array(
    'p1' => 'Metropack INDONESIA is one of the companies engaged in the packaging industry that was established to contribute to meeting the huge market needs. Armed with knowledge and experience and passion, we are always ready to provide services to the needs of customers with high professional and commitment.',
    'p2' => 'The products we produce are Carton Box, Impraboard, Paper Pallet and Offset printing along with supporting components in packaging activities for Local or Exlsport including Heavy Duty Paper Board, Edge Protector, Corner Protector, Round Protector, Strapping Band, Wrapping, Sticker, Label , and other printed products and promotional tools / Brand Development / Advertising, which are divided into 3 divisions, namely:',
    'p3' => 'As a company that provides Onestop Packaging Services, we collaborate with <strong> Storopack </strong> which is engaged in Protection & Cushioning to complement our performance in serving the needs of Customers and Prospective Customers.',

    'visih1' => 'VISI',
    'visi1' => 'Being one of the companies that always exist and develop even the best in retail and party scale packaging industries, to continue to help and improve the effectiveness and efficiency of customer packaging activities in a consistent and high commitment manner.',

    'misih1' => 'MISI',
    'misi1' => 'Develop competent employees in creating a good working atmosphere and optimal productivity. Continue to provide inspiration to create new innovations with creative ideas to produce quality products according to market needs. Presenting solutions as one form or form of contribution / service that we offer to customers. Expand the marketing network both nationally and internationally. Establish good cooperation and communication relationships with suppliers and customers for the realization of long-term cooperation.',
);
?>