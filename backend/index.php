<?php 

define('LOGIN','Login');
define('PASSWORD','Password');
define('EMAIL','Email');
define('INFO_REMEMBER_ME','Remember me');
define('MENU_FORGOT_PASSWORD','Forgot Password');
define('MENU_REGISTER','Register');

if (isset($_POST)) $post = $_POST;
if (isset($_GET)) $get = $_GET;

function base_url() {
	return 'a';
}

function debug($str, $die = 1) 
{
	echo "<pre>";
	print_r($str);
	echo "</pre>";
	if ($die) die;
}

function dologin()
{
	$post = $get = NULL;
	if ($_POST) {
		$post = $_POST;
		
		debug($post,1);
	}
	
}

dologin();

?>
<html>

<head>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>

</head>

<body>
<section class="bgPch">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 h600" style="padding-top:100px">
			
				<div class="col-sm-3">
				&nbsp;
				</div>
				<div class='col-sm-6'>
					<h1 class="title-header clrOrg">&nbsp;<?php echo LOGIN?></h1><hr/>
					<div class="errLog"></div>
					<form class='form-horizontal' role='form' method='post'>
						<div class='form-group form-group-sm'>
							<div class='col-sm-12'><input type='email' class='form-control' name='l_email' id='l_email' placeholder='<?php echo EMAIL?>' value='<?php if (isset($post['l_email'])) echo $post['l_email'] ; else if (isset($get['email'])) echo urldecode($get['email']); ?>' required></div>
						</div>
						<div class='form-group form-group-sm'>
							<div class='col-sm-12'><input type='password' class='form-control' name='l_password' id='l_password' placeholder='<?php echo PASSWORD?>' required></div>
						</div>
						<div class='form-group form-group-md'>
							<div class='col-sm-1 checkbox ' style='padding-left:30px'>
								<label><input type='checkbox' name='l_RememberMe' id='l_RememberMe' value='1' /></label>
							</div>
							<div class='col-sm-11' style='padding-top:8px'><label for='l_RememberMe'><?php echo INFO_REMEMBER_ME?></label></div>
						</div>
						<div class='form-group'>
							<div class='col-sm-12'>
								<div class='talRgt'><a href="<?php echo base_url()?>forgotpassword"><?php echo MENU_FORGOT_PASSWORD?></a> | <a href="<?php echo base_url().''?>"><?php echo MENU_REGISTER?></a></div>
							<input type="hidden" name="hdnLogin" value="1"/><button type="submit" value="" class="btn btn-success"/><?php echo LOGIN?></button></div>
						</div>
					</form>
					
					<?php
					if (isset($gInfo)) echo print_message($gInfo);
					?>
				</div>
				<div class="col-sm-3">
				&nbsp;
				</div>
			</div>
		</div>
	</div>
</section>

</body>
</html>